let arr = [1, 2, 3, 4, 5];
// let arr = [-1, -2, -3, -4, -5];

const multiplyBy = (arr, num) => {
  return arr.map((item) => item * num);
};

console.log(multiplyBy(arr, 5));
